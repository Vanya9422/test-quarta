<?php

namespace Database\Seeders;

use App\Imports\ProductImport;
use App\Services\Converters\ProductXmlConverter;
use App\Services\ImportService;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $importService = new ImportService();

        $productConverter = new ProductXmlConverter();

        $importService->importFromXml(
            'https://quarta-hunt.ru/bitrix/catalog_export/export_Ngq.xml',
            ProductImport::class,
            $productConverter
        );
    }
}

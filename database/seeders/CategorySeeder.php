<?php

namespace Database\Seeders;

use App\Imports\CategoryImport;
use App\Services\Converters\CategoryXmlConverter;
use App\Services\ImportService;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $importService = new ImportService();

        // Импорт категорий
        $categoryConverter = new CategoryXmlConverter();

        $importService->importFromXml(
            'https://quarta-hunt.ru/bitrix/catalog_export/export_Ngq.xml',
            CategoryImport::class,
            $categoryConverter
        );
    }
}

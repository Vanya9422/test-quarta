<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->boolean('available')->default(true);
            $table->string('url');
            $table->integer('price');
            $table->integer('old_price')->nullable();
            $table->string('picture')->nullable(); // я всегда исползую Пакет спати для картионок н в этом заании буду испозовать ссылки которие вы предоставили
            $table->string('vendor');
            $table->string('currency'); // я бы создал currencies по одельно таблице
            $table->foreignId('category_id')->constrained()->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};

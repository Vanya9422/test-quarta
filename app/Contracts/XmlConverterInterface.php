<?php

namespace App\Contracts;

interface XmlConverterInterface
{
    /**
     * @param object $xml
     * @param string $csvPath
     * @return void
     */
    public function convertToCsv(object $xml, string $csvPath): void;
}

<?php

namespace App\Services;

use App\Contracts\XmlConverterInterface;
use Maatwebsite\Excel\Facades\Excel;

class ImportService
{
    public function importFromXml($xmlPath, $importClass, XmlConverterInterface $converter)
    {
        $xml = simplexml_load_file($xmlPath);

        // Создание пути для временного CSV файла
        $csvPath = tempnam(sys_get_temp_dir(), 'csv');

        // Преобразование XML в CSV с использованием конвертера
        $converter->convertToCsv($xml, $csvPath);

        // Импорт данных из CSV файла
        Excel::import(new $importClass, $csvPath);

        // Удаление временного CSV файла
        unlink($csvPath);
    }
}

<?php

namespace App\Services\Converters;

use App\Contracts\XmlConverterInterface;
use App\Models\Category;

class CategoryXmlConverter implements XmlConverterInterface
{
    /**
     * Преобразует данные XML в CSV файл.
     *
     * @param object $xml Объект XML для конвертации.
     * @param string $csvPath Путь для сохранения CSV файла.
     */
    public function convertToCsv(object $xml, string $csvPath): void
    {
        // Открываем файл CSV для записи
        $csvFile = fopen($csvPath, 'w');

        // Получаем имена заполняемых полей модели Category
        $fields = (new Category())->getFillable();

        // Добавляем заголовки в CSV файл. Эти заголовки соответствуют полям модели Category
        fputcsv($csvFile, $fields);

        // Перебираем каждую категорию в XML и записываем её данные в CSV
        foreach ($xml->shop->categories->category as $category) {
            fputcsv($csvFile, [
                (int)$category['id'],
                (string)$category,
                isset($category['parentId']) ? (int)$category['parentId'] : null
            ]);
        }

        // Закрываем файл CSV
        fclose($csvFile);
    }
}

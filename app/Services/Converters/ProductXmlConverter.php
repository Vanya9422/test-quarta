<?php

namespace App\Services\Converters;

use App\Contracts\XmlConverterInterface;
use App\Models\Product;

class ProductXmlConverter implements XmlConverterInterface
{
    /**
     * Конвертирует XML в CSV файл.
     *
     * @param object $xml Объект XML для конвертации.
     * @param string $csvPath Путь для сохранения CSV файла.
     */
    public function convertToCsv(object $xml, string $csvPath): void
    {
        // Открытие файла CSV для записи
        $csvFile = fopen($csvPath, 'w');

        // Получение заполняемых (fillable) полей из модели Product
        $fields = (new Product())->getFillable();

        // Запись заголовков столбцов в CSV файл
        fputcsv($csvFile, $fields);

        // Перебор каждого предложения (offer) в XML и запись в CSV
        foreach ($xml->shop->offers->offer as $offer) {
            fputcsv($csvFile, [
                (int) $offer['id'],         // Преобразование ID в integer
                (string) $offer->name,      // Имя продукта
                (int) $offer->available,    // Доступность (преобразование в integer)
                (string) $offer->url,       // URL продукта
                (int) $offer->price,        // Цена продукта (преобразование в integer)
                (int) $offer->oldprice,     // Старая цена (преобразование в integer)
                (string) $offer->picture,   // Ссылка на изображение продукта
                (string) $offer->vendor,    // Производитель или поставщик продукта
                (string) $offer->currencyId,// ID валюты
                (int) $offer->categoryId    // ID категории (преобразование в integer)
            ]);
        }

        // Закрытие файла CSV
        fclose($csvFile);
    }
}

<?php

namespace App\Imports;

use App\Models\Category;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithUpserts;

class CategoryImport implements ToModel, WithChunkReading, WithUpserts, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Category([
            'id' => $row['id'],
            'name' => $row['name'],
            'parent_id' => $row['parent_id'] ?? null
        ]);
    }

    /**
     * @return int
     */
    public function chunkSize(): int
    {
        return 300;
    }

    /**
     * @return string|array
     */
    public function uniqueBy(): array|string
    {
        return 'id';
    }
}

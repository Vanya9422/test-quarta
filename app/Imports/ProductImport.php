<?php

namespace App\Imports;

use App\Models\Product;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithUpserts;

class ProductImport implements ToModel, WithHeadingRow, WithChunkReading, WithUpserts
{
    public function model(array $row)
    {
        return new Product([
            'id' => $row['id'],
            'name' => $row['name'],
            'available' => $row['available'],
            'url' => $row['url'],
            'price' => $row['price'],
            'old_price' => $row['old_price'],
            'picture' => $row['picture'],
            'vendor' => $row['vendor'],
            'currency' => $row['currency'],
            'category_id' => $row['category_id'],
        ]);
    }

    public function chunkSize(): int
    {
        return 100;
    }

    /**
     * @return string|array
     */
    public function uniqueBy(): array|string
    {
        return 'id';
    }
}
